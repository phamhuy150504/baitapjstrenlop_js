    // Class Objec Sinh Viên

    function Student (_maSV, _tenSV, _email, _matKhau, _date, _course, _toan, _ly, _hoa) {
        this.MaSV = _maSV;
        this.TenSV = _tenSV;
        this.Email = _email;
        this.PassWord = _matKhau;
        this.Date = _date;
        this.Course = _course;
        this.Toan = _toan;
        this.Ly = _ly;
        this.Hoa = _hoa;
        this.DTB = this.DiemTrungBinh()
    }
    // Student.prototype.DTB = 0
    Student.prototype.DiemTrungBinh = function () {
        this.DTB = (this.Toan*1 + this.Ly*1 + this.Hoa*1) / 3
        return  this.DTB
    }
