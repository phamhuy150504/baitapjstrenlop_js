    // The function of form

    function ListF () {
        this.AddSV = function (DSSV, id) {
            let content = ''
            DSSV.forEach(element => {
                content += 
                `
                <tr>
                    <td>${element.MaSV}</td>
                    <td>${element.TenSV}</td>
                    <td>${element.Email}</td>
                    <td>${element.Date}</td>
                    <td>${element.Course}</td>
                    <td>${element.DTB}</td>
                    <td>
                    <button onclick="DeleteStudent('${element.MaSV}')" class="btn btn-danger">Xóa</button>
                    <button onclick="SuaSinhVien('${element.MaSV}')" class="btn btn-warning btn">Sửa</button>
                    </td>
                </tr>
                `
            });
            let dom = document.getElementById(id).innerHTML = content
            return dom
        }
        this.DeleteSV = function (DSSV, maSv) {
            for(var i = 0; i < DSSV.length; i++) {
                if (DSSV[i].MaSV == maSv) {
                    DSSV.splice(i, 1)
                }
            } 
            return DSSV
        }
        this.FixSv = function (DSSV, maSv) {
            for (var i = 0; i < DSSV.length; i++) {
                if (DSSV[i].MaSV == maSv) {
                    DomId('maSV').value = DSSV[i].MaSV
                    DomId('maSV').disabled = true
                    DomId('tenSv').value = DSSV[i].TenSV
                    DomId('emailSV').value = DSSV[i].Email
                    DomId('password').value = DSSV[i].PassWord
                    DomId('date').value = DSSV[i].Date
                    DomId('course').value = DSSV[i].Course
                    DomId('toan').value = DSSV[i].Toan
                    DomId('ly').value = DSSV[i].Ly
                    DomId('hoa').value = DSSV[i].Hoa
                }
            }
            
        }
        this.TimViTri = function (DSSV, maSV) {
            let viTri = DSSV.findIndex(function (item) {
                return item.MaSV == maSV
            })
            return viTri
        };
    }