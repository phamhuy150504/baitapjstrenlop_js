    // array ListSV
    let arrStudent = [];

    // List Function
    let ChucNang = new ListF()

    // get Storage Json
    let getJson = localStorage.getItem('DSSV');

    if (getJson != null) {
        let dataJson = JSON.parse(getJson)

        arrStudent = dataJson.map(function(item) {
            return new Student(item.MaSV, item.TenSV, item.Email, item.PassWord, item.Date, item.Course, item.Toan, item.Ly, item.Hoa)
        });

        ChucNang.AddSV(arrStudent, 'tbody')
    }

    // function DomId
    function DomId(id) 
    {
        let dom = document.getElementById(id)
        return dom;
    }

    // function get information student
    function getInformation () 
    {
        let maSv = DomId('maSV').value;
        let tenSv = DomId('tenSv').value;
        let emailSv = DomId('emailSV').value;
        let passwordSv = DomId('password').value;
        let dateSv = DomId('date').value;
        let courseSv = DomId('course').value;
        let diemToan = DomId('toan').value;
        let diemLy = DomId('ly').value;
        let diemHoa = DomId('hoa').value;

        return new Student(maSv, tenSv, emailSv, passwordSv, dateSv, courseSv, diemToan, diemLy, diemHoa)
    }
    
    // function add Sv 
    function ThemSinhVien() 
    {   
        let SinhVien = getInformation()
        arrStudent.push(SinhVien)
        ChucNang.AddSV(arrStudent, 'tbody')
        Json(arrStudent)
    }
    
    // Json
    function Json(DSSV) {
        localStorage.setItem('DSSV', JSON.stringify(DSSV))
    }

    // function delete Student
    function DeleteStudent (maSv) {
        ChucNang.AddSV(ChucNang.DeleteSV(arrStudent, maSv), 'tbody')
        Json(arrStudent)
    }

    // fix Sv 
    function SuaSinhVien (id) {
        ChucNang.FixSv(arrStudent, id)
    }

    // function CapNhap SinhVien
    function CapNhap() {
        let maCN = DomId('maSV').value;
        console.log(maCN);
        let viTri = ChucNang.TimViTri(arrStudent, maCN)
        if (viTri != -1) {
            arrStudent[viTri].TenSV = DomId('tenSv').value
            console.log(arrStudent[viTri].TenSV);
            console.log(DomId('tenSv').value)
            arrStudent[viTri].Email = DomId('emailSV').value
            arrStudent[viTri].PassWord = DomId('password').value
            arrStudent[viTri].Date = DomId('date').value
            arrStudent[viTri].Course = DomId('course').value
            arrStudent[viTri].Toan = DomId('toan').value
            arrStudent[viTri].Ly = DomId('ly').value
            arrStudent[viTri].Hoa = DomId('hoa').value
            arrStudent[viTri].DTB = (arrStudent[viTri].Toan*1 + arrStudent[viTri].Ly*1 + arrStudent[viTri].Hoa*1) /3
        }
        ChucNang.AddSV(arrStudent, 'tbody')
    }

    // function SearchSv
    function TimKiemNhanVien() {
        let tuKhoa = DomId('search-Sv').value;

        let arrSvSearch = []
        arrStudent.forEach(function (sinhVien) {
            if(sinhVien.MaSV.toLowerCase().trim().search(tuKhoa.toLowerCase().trim()) != -1) 
            {
            arrSvSearch.push(sinhVien)
            }
        })  
        ChucNang.AddSV(arrSvSearch, 'tbody')
    }